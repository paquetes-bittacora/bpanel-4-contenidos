# Asociar un Content a un modelo

Para que un Model cualquiera de otros módulos tenga asociado un Content, se puede
llamar a `ContentFacade::associateWithModel($model)`, que se encargará de hacer la
asociación si no existe ya. Por ejemplo, guardando un `PageModel`:

```php
$page = new PageModel($request->all());
$page->save();

ContentFacade::associateWithModel($page);
```

# Añadir relación con contenido a un modelo

Si se necesita acceder al Content relacionado con un modelo desde el modelo, se
puede usar el trait `\Bittacora\Content\Traits\RelatesToContentTrait`, que añadirá
el método `content()` al modelo correspondiente.