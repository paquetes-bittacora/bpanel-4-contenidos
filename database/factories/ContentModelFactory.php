<?php

namespace Bittacora\Content\Database\Factories;

use Bittacora\Content\Models\ContentModel;
use Illuminate\Database\Eloquent\Factories\Factory;


class ContentModelFactory extends Factory
{
    protected $model = ContentModel::class;

    public function definition()
    {
        return [

        ];
    }
}
