<?php

namespace Bittacora\Content\Commands;

use Illuminate\Console\Command;

class ContentCommand extends Command
{
    public $signature = 'content';

    public $description = 'My command';

    public function handle()
    {
        $this->comment('All done');
    }
}
