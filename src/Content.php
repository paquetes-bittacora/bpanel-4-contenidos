<?php

namespace Bittacora\Content;

use Bittacora\Content\Models\ContentModel;
use Bittacora\ContentMultimedia\ContentMultimediaFacade;
use Bittacora\ContentMultimedia\Models\ContentMultimedia;
use Bittacora\ContentMultimedia\Models\ContentMultimediaImages;
use Bittacora\ContentMultimediaDocuments\Models\ContentMultimediaDocumentsModel;
use Bittacora\ContentMultimediaImages\Models\ContentMultimediaImagesModel;
use Bittacora\Multimedia\MultimediaFacade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Content
{
    /**
     * Asocia un Content a un modelo si no lo tiene asociado ya.
     *
     * @param Model $model El modelo al que se quiere asociar un Content.
     */
    public function associateWithModel(Model $model)
    {
        if (!$model->content()->exists()) {

            $content = new ContentModel();
            $model->content()->save($content);

        }
    }

    /**
     * Borra el Content de un Model
     * @param Model $model
     */
    public function deleteModelContent(Model $model)
    {
        if ($model->content()->exists()) {
            $model->content()->delete();
        }
    }

    /**
     * @deprecated
     */
    public function associateWithMultimedia(Model $model, array $files)
    {
        trigger_error('Para añadir multimedia a un modelo, es preferible usar
         ContentMultimedia::uploadModelMultimedia. Ver readme de bittacora/bpanel4-content-multimedia', E_USER_DEPRECATED);

        $filesCount = count($files['name']);

        for($i = 0; $i < $filesCount; $i++){
            $file = new UploadedFile(
                $files['tmp_name'][$i],
                $files['name'][$i]
            );

            $file = ContentMultimediaFacade::singleUpload($file);

            if($file){
                $fileExtension = $file->mediaModel()->first()->extension;

                if(in_array($fileExtension, MultimediaFacade::getImageExtensions())){
                    $contentMultimediaModel = new ContentMultimediaImagesModel();
                }elseif(in_array($fileExtension, MultimediaFacade::getDocumentExtensions())){
                    $contentMultimediaModel = new ContentMultimediaDocumentsModel();
                }else{
                    $type = 'C';
                }

                $contentMultimediaModel->fill([
                    'content_id' => $model->content->id,
                    'multimedia_id' => $file->id,
                ]) -> save();
            }
        }
    }
}
