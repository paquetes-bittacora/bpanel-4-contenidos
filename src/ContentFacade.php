<?php

namespace Bittacora\Content;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Content\Content
 */
class ContentFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Content::class;
    }
}
