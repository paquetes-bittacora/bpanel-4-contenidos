<?php

namespace Bittacora\Content;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Content\Commands\ContenidosCommand;

class ContentServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('bpanel4-contenidos')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_bpanel4-contenidos_table')
            ->hasCommand(ContenidosCommand::class);
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
