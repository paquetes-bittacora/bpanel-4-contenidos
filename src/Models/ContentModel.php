<?php

namespace Bittacora\Content\Models;

use Bittacora\Multimedia\Models\Multimedia;
use Illuminate\Database\Eloquent\Model;

class ContentModel extends Model
{
    protected $table = 'contents';

    public function contentable()
    {
        return $this->morphTo();
    }

    public function multimedia(){
        return $this->hasMany(ContentMultimedia::class, 'content_id');
    }

}
