<?php

namespace Bittacora\Content\Traits;

use Bittacora\Content\Models\ContentModel;

trait RelatesToContentTrait
{
    public function content()
    {
        return $this->morphOne(ContentModel::class, 'model');
    }
}
